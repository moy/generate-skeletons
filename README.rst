=====================================================================================
 generate-skeletons: a simple skeleton generator (designed by teachers for teachers)
=====================================================================================

generate-skeletons.py: extract student version from the teacher's version
-------------------------------------------------------------------------

``generate-skeletons.py`` is a simple python program to extract skeleton
(i.e. incomplete code to be distributed to students) from a full
source code. For example, you write::

    int fact (int x) {
            /* UNCOMMENT return fact(x); */
            /* BEGIN CUT */
            if (x <= 1) {
                    return 1;
            } else {
                    return x * fact(x - 1);
            }
            /* END CUT */
    }

Then run ``generate-skeletons.py`` and get automatically::

    int fact (int x) {
            return fact(x);
    }

Alternatively, you may want to extract an unannoted version (typically
to get the solution of the exercise) using the ``--strip`` option::

    /* An almost realistic example of exercise. */
    
    #include <stdio.h>
    
    int fact (int x) {

            if (x <= 1) {
                    return 1;
            } else {
                    return x * fact(x - 1);
            }
    }
    
    int main (int argc, char ** argv) {
            int x;
            scanf("%d", &x);
            printf("Fact(%d) = %d\n", x, fact(x));
            int i;
            for (i = 0; i <= 10; i++)
                    printf("Fact(%d) = %d\n", i, fact(i));
            return 0;
    }

Extract code excerpts to separate files with ``--excerpts``
-----------------------------------------------------------

If you've already fought with LaTeX's ``\lstinputlisting`` and its
``firstline=`` and ``lastline=`` options to get the right piece of
code to inclued, I've got good news!

Mark pieces of code to extract directly in the source file::

   #include <stdio.h>

   // BEGIN COPY TO (main.c)
   int main() {
       // BEGIN CUT
       printf("This is cut!\n");
       // END CUT
       return 0;
   }
   // END COPY TO (main.c)

``generate_skeletons.py`` will generate a separate file (``main.c``
here) with pieces of code marked by ``BEGIN COPY TO`` /
``END COPY TO`` directives (hence, without the ``#include`` line in
particular), that you can use directly without bothering with line
numbers::

  \lstinputlisting{main.c}

This is disabled by default, and needs to be enabled with
``--excerpts``.

You can use as many ``COPY TO`` directives as you want to split your
code into files. You may even nest or interleave ``COPY TO``
directives.

Other directives are processed as usual within ``COPY TO`` portions,
so the above example will generate only a ``return 0;`` in ``main.c``
by default, but will generate also the ``printf`` line if you process
it with ``--strip``.

Dealing with pathnames (files, directories)
-------------------------------------------

By default, when using ``--output-dir``, ``generate-skeletons.py``
will preserve the directory structure of the source, i.e.::

  generate-skeletons.py 1/2/3/4/* --output-dir output

will generate files in ``output/1/2/3/4/*``. You can change this
behavior either by ``cd``-ing to another place before calling
``generate-skeletons.py``, or using ``--strip-components``, like
this::

  generate-skeletons.py 1/2/3/4/* --output-dir output --strip-components=3

that will take away the first 3 components (``1/2/3`` in our case)
from the source path when choosing when to generate files
(``output/4/*`` in our case).

diff2skell.py: get annotated version from solution and student's code
---------------------------------------------------------------------

You can also generate an annotated "teacher's version" file using the
``patch2skel.py`` script.

Cut the skeleton out of the solution
....................................

One use-case is to cut a skeleton from the solution. Instead of
working directly with annotation, one can modify the code until it
looks like a proper skeleton. For example, using Git::

  $ ... edit solution
  $ git commit -a -m "Solution"
  [master 15629c3] Solution
  $ ... remove teachers-only code
  $ git rm teachers-only-file  # if needed
  $ git commit -a -m "Skeleton"
  [master 51109e9] Skeleton
  $ git diff 15629c3 51109e9 | diff2skel.py --stdout | patch -R -p 1

``diff2skel.py`` rewrites the patch produced by ``git diff``, and
``patch -R`` applies the result. Alternatively, one can use ``git
apply`` instead of ``patch``, and a convenience shortcut ``-i --git``
is provided to pipe the result into ``git apply -R -v --index``::

  $ git diff 15629c3 51109e9 | diff2skel.py -i --git
  $ git commit -m "Skeletized version"

Depending on the language you're using, you may want different style
of annotation (``# BEGIN CUT`` Vs ``// BEGIN CUT`` Vs ...). Pick one
with ``--style=...``::

  $ git diff 15629c3 51109e9 | diff2skel.py -i --git --style=python

(The list of available styles is given by ``diff2skell.py --help``)

Write the solution from a skeleton
..................................

A second use-case is to write the skeleton first, then write the full
solution, and mark the pieces to remove in the skeleton afterwards.
This can be done with::

  $ ... edit skeleton
  $ git commit -a -m "Skeleton"
  [master deadbeef] Skeleton
  $ ... add code for full solution
  $ git commit -a -m "Solution"
  [master c0ffee12] Solution
  $ git checkout deadbeef -- file_to_skeletify
  $ git diff --staged | diff2skel.py --git --inplace
  $ git diff --staged
  # Examine the result
  $ git commit -m "Skeletified version"

The same recipe can be used when adding a new exercise, i.e. a new
piece of code in the solution, but that should not appear in the
skeleton.

Install
=======

To download and install, run::

  pip3 install --user git+https://gitlab.com/moy/generate-skeletons

If needed, add ``~/.local/bin`` to your ``$PATH``.

Alternatively, just download the file ``generate-skeletons.py``. It's
self-contained and has no dependency other than Python and its
standard library.

To use ``diff2skell.py``, one needs the `unidiff
<https://pypi.org/project/unidiff/>`__ package, installable with::

  pip3 install unidiff

Annotation language
===================

``generate-skeletons.py`` uses annotations as comments in source files.
The comment syntax for most languages is supported:

* ``/* ... */`` and ``// ...`` for C-like languages

* ``# ...`` for Python, Perl, Shell

* ``-- ...`` for Ada and VHDL

* ``(* ... *)`` for Caml and Coq (no nested comment support, sorry).

* ``<!-- ... -->`` for HTML and Markdown

The following annotations are supported:

* ``BEGIN CUT`` / ``END CUT``: lines between these annotations will be
  removed. Typical example::

    def f():
        # BEGIN CUT
	skeletized_part(that, will, be, removed)
	# END CUT

* ``# UNCOMMENT ...`` or ``/* UNCOMMENT ... */``: the line will be
  preserved, and the ``...`` part will be uncommented. Typical
  example::

    # BEGIN CUT
    skeletized()
    # END CUT
    # UNCOMMENT raise Exception('Not yet implemented')

* Similarly, for languages supporting multi-line comments, one may
  use::

    /* BEGIN UNCOMMENT
    this piece of code will be uncommented
    (i.e. this is the student's code,
    commented out in the source file)
       END UNCOMMENT */

  or, for Caml/Coq::

    (* BEGIN UNCOMMENT
    This will be uncommented
    END COMMENT *)

  Using ``--strip``, the whole comment block will be removed.

* ``TO DELETE`` to mark lines to be removed individually (equivalent
  to surrounding the line with ``BEGIN CUT``/``END CUT`` annotations).

* ``EXCLUDED FROM SKELETON`` to exclude a complete file from a
  skeleton. Note that this annotation is ignored when ``--strip`` is
  used (``--strip`` is meant to generate a clean solution, including
  all files).

Example annotated files can be found in the `<tests/>`__ directory.

Several skeleton versions in one file
=====================================

One can make several versions of the same skeleton in the same source
file, for example::

  # BEGIN CUT (easy)
  skeletized()
  # END CUT
  # UNCOMMENT (easy) raise Exception('Not yet implemented')
  # BEGIN CUT
  really_hard_piece_of_code()
  # END CUT
  # UNCOMMENT raise Exception('Not yet implemented (and it will be hard)')

Note the version annotations ``(easy)`` on some annotations. By
default, this will yield::

  raise Exception('Not yet implemented')
  raise Exception('Not yet implemented (and it will be hard)')

However, you can also call ``generate-skeletons.py`` with
``--cut-only easy`` and get::

  raise Exception('Not yet implemented')
  really_hard_piece_of_code()

Only the annotations marked ``(easy)`` are actually removed, the other
annotations (marked with a different version, or not marked with a
version) are stripped by the code is kept. To strip also annotations
not marked with a version, use ``--cut-unversioned`` in addition to
``--cut-only``.

``UNCOMMENT`` annotations are special:

* ``# UNCOMMENT (v)`` is uncommented with ``--cut-only v``, or when
  ``--cut-only`` is not in action.

* ``# UNCOMMENT (v)`` is removed completely when ``--cut-only`` is in
  action, but ``v`` is not in the list of versions to cut. In
  particular, all ``# UNCOMMENT (v)`` are cut when ``--cut-only -`` is
  used.

For an example using versions annotations, see
`<tests/example_versions.c>`__.

Version annotations can contain only alphanumeric, dots and
underscores (e.g. ``(easy)``, ``(very_hard)`` or ``(1.0)``, but not
``(x,y)``). If there's ambiguity between actual code and a version,
add an empty version, like::

  /* UNCOMMENT() (this_is_actual_code) */

Usage
=====

Run ``generate-skeletons.py --help`` and ``patch2skel.py --help`` to
get help.
