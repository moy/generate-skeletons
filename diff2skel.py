#!/usr/bin/env python

# Find the latest version of this program at
# https://gitlab.com/moy/generate-skeletons


# BSD 2-Clause License
#
# Copyright (c) 2019, Matthieu Moy
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function

import re
import sys
import argparse
import subprocess
from unidiff import PatchSet


def make_parser():
    parser = argparse.ArgumentParser(description="""
        Read a diff file on stdin and produce a file or patch with
        skeleton annotations.""")
    parser.add_argument(
        "--inplace", "-i",
        help="Apply changes directly. "
        "Implies --destination except if --git is provided.",
        default=False,
        action='store_true')
    parser.add_argument(
        "--stdout",
        help="Output diff (to be applied with patch -R) to stdout",
        default=False,
        action='store_true')
    parser.add_argument(
        "--git",
        help="Use Git to apply the resulting patch with --inplace",
        default=False,
        action='store_true')
    parser.add_argument(
        "--output", '-o',
        help="Output patch file.",
        metavar="FILE", type=str)
    parser.add_argument(
        "--strip", '-p',
        help="Strip first parts of file names (like patch -p).",
        default=0,
        metavar="N", type=int)
    parser.add_argument(
        "--destination",
        help="Use destination file as patch header (--- and +++)",
        default=False,
        action='store_true')
    parser.add_argument(
        '--style', type=str,
        default='c++',
        choices=['c', 'c++', 'ada', 'caml', 'python'],
        help='Comment style: /* c */, // c++, -- ada, (* caml *), # python')

    return parser


def die(*msg):
    # Don't mix stdout and stderr
    sys.stdout.flush()
    print(*msg, file=sys.stderr)
    sys.exit(1)


def parse_cli():
    parser = make_parser()
    args = parser.parse_args()
    if not args.stdout and not args.inplace and not args.output:
        die("At least one of --output/-o, --inplace and --stdout"
            " should be provided.")
    if args.inplace and not args.git:
        args.destination = True
    return args


class LangHelper():
    @staticmethod
    def get_lang_helper(style):
        return {
            'c': CHelper,
            'c++': CppHelper,
            'ada': AdaHelper,
            'caml': CamlHelper,
            'python': PythonHelper
        }[style]()

    def split_indent(self, line):
        """For a line like
        <indentation><code>
        return (indentation, v, code), where v is '' if the code does not look
        like a version annotation, and '()' if it does.
        """
        plausible_version = re.match(
            r'^ *(\((?P<version>[a-zA-Z0-9\._]*)\))', line)
        v = ""
        if plausible_version:
            # The line starts with something that may be a version, eg.
            # (foo), and we don't want to emit UNCOMMENT (foo) because the
            # (foo) part would be stripped away. Instead, output an empty
            # version to avoid ambiguity like UNCOMMENT() (foo).
            v = "()"
        m = re.match(r"^(\s*)(.*)\n$", line)
        return m.group(1), v, m.group(2)

    def get_indent(self, line):
        m = re.match(r"^([ \t]*)(.*)$", line)
        return m.group(1)


class PrefixHelper(LangHelper):
    def uncomment(self, line):
        i, v, c = self.split_indent(line)
        return i + self.prefix + " UNCOMMENT" + v + " " + c

    def begin_cut(self):
        return self.prefix + " BEGIN CUT"

    def end_cut(self):
        return self.prefix + " END CUT"


class SurroundedHelper(LangHelper):
    def uncomment(self, line):
        i, v, c = self.split_indent(line)
        # Add a trailing space only if it wasn't already there
        space = '' if re.match(r'\s$', " " + c) else ' '
        return (
            i +
            self.prefix + " UNCOMMENT" +
            v + " " + c +
            space + self.postfix
        )

    def begin_cut(self):
        return self.prefix + " BEGIN CUT " + self.postfix

    def end_cut(self):
        return self.prefix + " END CUT " + self.postfix


class CHelper(SurroundedHelper):
    prefix = "/*"
    postfix = "*/"


class CppHelper(PrefixHelper):
    prefix = "//"


class AdaHelper(PrefixHelper):
    prefix = "--"


class CamlHelper(SurroundedHelper):
    prefix = "(*"
    postfix = "*)"


class PythonHelper(PrefixHelper):
    prefix = "#"


class PatchProcessor():
    def __init__(self, patch_set, lang_helper, args):
        self.lang_helper = lang_helper
        self.patch_set = patch_set
        self.in_cut = False
        self.current_indent = ''
        self.args = args

    def get_offset(self):
        offset = self.offset
        self.offset += self.next_offset
        self.next_offset = 0
        return offset

    @staticmethod
    def file_exists(timestamp, name):
        if timestamp is not None and timestamp.startswith("1970-01-01 "):
            return False
        if name == "/dev/null":
            return False
        return True

    def process_patch(self):
        output = ""
        for patched_file in self.patch_set:
            self.offset = 0
            self.next_offset = 0
            target_file = patched_file.target_file
            source_file = patched_file.source_file
            if self.args.destination:
                if patched_file.target_file == "/dev/null":
                    target_file = source_file
                else:
                    source_file = target_file

            source = "--- %s%s\n" % (
                source_file,
                '\t' + patched_file.source_timestamp
                if patched_file.source_timestamp else '')
            target = "+++ %s%s\n" % (
                target_file,
                '\t' + patched_file.target_timestamp
                if patched_file.target_timestamp else '')
            output += source + target
            if not self.file_exists(patched_file.target_timestamp,
                                    patched_file.target_file):
                # file was deleted
                body = "-// EXCLUDED FROM SKELETON\n"
                first = True
                for patch_hunk in patched_file:
                    for line in patch_hunk:
                        body += "-" + line.value
                    if first:
                        # Account for the // EXCLUDED line
                        patch_hunk.source_length += 1
                        self.next_offset += 1
                        first = False
                    hunk_head = "@@ -%d,%d +%d,%d @@\n" % (
                        patch_hunk.source_start + self.get_offset(),
                        patch_hunk.source_length,
                        patch_hunk.target_start, patch_hunk.target_length
                        )
                output += hunk_head + body
            elif not self.file_exists(patched_file.source_timestamp,
                                      patched_file.source_file):
                # file was created
                body = ""
                for patch_hunk in patched_file:
                    for line in patch_hunk:
                        body += (
                            "+" +
                            self.lang_helper.uncomment(line.value) +
                            "\n"
                        )
                        patch_hunk.source_length += 1
                        self.next_offset += 1
                    hunk_head = "@@ -%d,%d +%d,%d @@\n" % (
                        patch_hunk.source_start + self.get_offset(),
                        patch_hunk.source_length,
                        patch_hunk.target_start, patch_hunk.target_length
                        )
                output += hunk_head + body
            else:
                for patch_hunk in patched_file:
                    output += self.process_hunk(patch_hunk)
        return output

    def process_hunk(self, patch_hunk):
        hunk_body = ""
        for line in patch_hunk:
            hunk_body += self.process_line(patch_hunk, line)
        if self.in_cut:
            hunk_body += (
                "-" +
                self.current_indent +
                self.lang_helper.end_cut() +
                "\n"
            )
            patch_hunk.source_length += 1
            self.next_offset += 1
            self.in_cut = False
        if patch_hunk.section_header:
            section_header = ' ' + patch_hunk.section_header
        else:
            section_header = ''
        hunk_head = "@@ -%d,%d +%d,%d @@%s\n" % (
            patch_hunk.source_start + self.get_offset(),
            patch_hunk.source_length,
            patch_hunk.target_start,
            patch_hunk.target_length,
            section_header)
        return hunk_head + hunk_body

    def process_line(self, patch_hunk, line):
        output = ""
        if not line.is_removed and self.in_cut:
            output += (
                "-" +
                self.current_indent +
                self.lang_helper.end_cut() +
                "\n"
            )
            patch_hunk.source_length += 1
            self.next_offset += 1
            self.in_cut = False

        if line.is_context:
            output += " " + line.value
        elif line.is_removed:
            if not self.in_cut:
                self.current_indent = self.lang_helper.get_indent(line.value)
                output += (
                    "-" +
                    self.current_indent +
                    self.lang_helper.begin_cut() +
                    "\n"
                )
                self.in_cut = True
                patch_hunk.source_length += 1
                self.next_offset += 1
            output += "-" + line.value
        elif line.is_added:
            output += "+" + line.value
            output += "-" + self.lang_helper.uncomment(line.value) + "\n"
            patch_hunk.source_length += 1
            self.next_offset += 1
        return output


def main():
    args = parse_cli()
    patch_str = sys.stdin.read()
    if hasattr(patch_str, 'decode'):
        patch_str = patch_str.decode(encoding='UTF-8')
    patch_set = PatchSet(patch_str)
    lang_helper = LangHelper.get_lang_helper(args.style)
    patch_processor = PatchProcessor(patch_set, lang_helper, args)
    output = patch_processor.process_patch()
    if hasattr(output, 'encode'):
        output = output.encode(encoding='UTF-8')
    if args.stdout:
        sys.stdout.buffer.write(output)
    if args.inplace:
        if args.git:
            cmd = ['git', 'apply', '-R', '-v', '--index']
        else:
            cmd = ['patch', '-p' + str(args.strip), '-R']
        p = subprocess.Popen(cmd,
                             stdin=subprocess.PIPE)
        p.stdin.write(output)
        p.stdin.close()
        p.wait()
    if args.output:
        with open(args.output, 'w') as f:
            f.write(output)


if __name__ == '__main__':
    main()
