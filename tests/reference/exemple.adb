with Ada.Text_Io, Ada.Integer_Text_IO;
use  Ada.Text_Io, Ada.Integer_Text_IO;

procedure Exemple is
   function Fact(N: Integer) return Integer is
   begin
      return Fact(N); -- A CHANGER.
   end;
   X : Integer;
begin
   Get(X);
   Put_Line("Fact(" & Integer'Image(X) & ") = " &
              Integer'Image(Fact(X)) & ".");
end Exemple;
