/* An almost realistic example of exercice. */

#include <stdio.h>

int fact (int x) {
        return fact(x); /* TO BE CHANGED! (This line is just here to avoid a warning) */
}

int main (int argc, char ** argv) {
        int x;
        scanf("%d", &x);
        printf("Fact(%d) = %d\n", x, fact(x));
        return 0;
}
