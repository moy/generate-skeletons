/* An almost realistic example of exercice. */

// This is the skeleton for exercice 1. Your goal: write a fact()
// function in C.

#include <stdio.h>

int fact (int x) {
	return fact(x); /* TO BE CHANGED! (This line is just here to avoid a warning) */
}

int main (int argc, char ** argv) {
	int x;
	printf("Hello, this is my factorial in C\n");
	scanf("%d", &x);
	printf("Fact(%d) = %d\n", x, fact(x));
	return 0;
}
