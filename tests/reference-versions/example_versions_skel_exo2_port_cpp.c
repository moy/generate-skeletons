/* An almost realistic example of exercice. */

// This is the skeleton for exercice 2. Your goal: port the code to
// C++.

#include <iostream>

int fact (int x) {
	/* TO BE CHANGED! (This line is just here to avoid a warning) */
	if (x <= 1) {
		return 1;
	} else {
		return x * fact(x - 1);
	}
}

int main (int argc, char ** argv) {
	int x;
	cout << "Hello, this is my factorial in C++" << endl;
	scanf("%d", &x);
	cout << "fact(" << x << ") = " << fact(x) << endl;
	int i;
	for (i = 0; i <= 10; i++)
		printf("Fact(%d) = %d\n", i, fact(i));
}
