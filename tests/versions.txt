This line is for everyone
# BEGIN CUT (v1)
This line is cut from v1
# END CUT
This line is also cut from v1 # TO DELETE (v1)
# UNCOMMENT This is the skeleton line for v1
/* BEGIN CUT (v2) */
This line is cut from v2
/* END CUT */
This line is also cut from v2 /* TO DELETE (v2) */
# UNCOMMENT This is the skeleton line for v2
# BEGIN CUT
This line is cut only when no --cut-only is active.
# END CUT
# UNCOMMENT This is the default skeleton line
// UNCOMMENT () Line without a version note either
// UNCOMMENT() Again a line without a version note
/* UNCOMMENT()Again a line without a version note (2) */
This line is deleted // TO DELETE
// BEGIN CUT
In cut block, but before UNCOMMENT part (cut by default)
// UNCOMMENT (v1) Line uncommented in v1, cut by default
In cut block, but after UNCOMMENT part (cut by default)
// END CUT
// BEGIN CUT (v1)
In cut block, but before UNCOMMENT part (cut in v1)
// UNCOMMENT (v2) Line uncommented in v2, cut in v1
In cut block, but after UNCOMMENT part (cut in v1)
// END CUT

First block:

// BEGIN CUT
In cut block, but before UNCOMMENT part (cut by default)
/* BEGIN UNCOMMENT (v1)
Block uncommented in v1, cut by default
   END UNCOMMENT */
In cut block, but after UNCOMMENT part (cut by default)
// END CUT

Next block:

// BEGIN CUT (v1)
/* BEGIN UNCOMMENT (v2)
In cut block, but before UNCOMMENT part (cut in v1)
Block uncommented in v2, cut in v1
   END UNCOMMENT */
In cut block, but after UNCOMMENT part (cut in v1)
// END CUT
