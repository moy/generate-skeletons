/* An almost realistic example of exercice. */

// BEGIN CUT (cpp)
// This is the skeleton for exercice 1. Your goal: write a fact()
// function in C.
// END CUT
// BEGIN CUT (c)
// This is the skeleton for exercice 2. Your goal: port the code to
// C++.
// END CUT

// BEGIN CUT (c)
#include <iostream>
// END CUT
// UNCOMMENT (c) #include <stdio.h>

int fact (int x) {
	/* UNCOMMENT return fact(x); */ /* TO BE CHANGED! (This line is just here to avoid a warning) */
	/* START CUT */
	if (x <= 1) {
		return 1;
	} else {
		return x * fact(x - 1);
	}
	/* END CUT */
}

int main (int argc, char ** argv) {
	int x;
	// UNCOMMENT (cpp) cout << "Hello, this is my factorial in C++" << endl;
	// UNCOMMENT (c) printf("Hello, this is my factorial in C\n");
	scanf("%d", &x);
	// BEGIN CUT (c)
	cout << "fact(" << x << ") = " << fact(x) << endl;
	// END CUT
	// UNCOMMENT(c) printf("Fact(%d) = %d\n", x, fact(x));
	int i; /* TO DELETE */
	for (i = 0; i <= 10; i++) /* TO DELETE */
		printf("Fact(%d) = %d\n", i, fact(i)); /* TO DELETE */
	/* BEGIN UNCOMMENT (c)
	return 0;
	END UNCOMMENT */
}
