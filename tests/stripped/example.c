/* An almost realistic example of exercice. */

#include <stdio.h>

int fact (int x) {
        /* TO BE CHANGED! (This line is just here to avoid a warning) */
        if (x <= 1) {
                return 1;
        } else {
                return x * fact(x - 1);
        }
}

int main (int argc, char ** argv) {
        int x;
        scanf("%d", &x);
        printf("Fact(%d) = %d\n", x, fact(x));
        int i;
        for (i = 0; i <= 10; i++)
                printf("Fact(%d) = %d\n", i, fact(i));
        return 0;
}
