with Ada.Text_Io, Ada.Integer_Text_IO;
use  Ada.Text_Io, Ada.Integer_Text_IO;

procedure Exemple is
   function Fact(N: Integer) return Integer is
   begin
      -- UNCOMMENT return Fact(N); -- A CHANGER.
      -- START CUT
      if N <= 1 then
         return 1;
      else
         return N * Fact(N-1);
      end if;
      -- END CUT
   end;
   X : Integer;
begin
   Get(X);
   Put_Line("Fact(" & Integer'Image(X) & ") = " &
              Integer'Image(Fact(X)) & ".");
   Put_Line("Fact(" & Integer'Image(1) & ") = " & -- TO DELETE
              Integer'Image(Fact(2)) & ".");      -- TO DELETE
   Put_Line("Fact(" & Integer'Image(2) & ") = " & -- TO DELETE
              Integer'Image(Fact(2)) & ".");      -- TO DELETE
end Exemple;
