#!/bin/sh

set -x
set -e

SCRIPT=$PWD/../generate-skeletons.py
DIFF2SKELL=$PWD/../diff2skel.py
SCRIPTDIR=$PWD

rm -fr tmp-check-diff2skel.$$
TMPDIR=tmp-check-diff2skel.$$
mkdir "$TMPDIR"

(
    cd "$TMPDIR"
    mkdir -p output-skeletized output-stripped output-rebuilt-skeletized output-rebuilt-stripped
)

# Skeletize files ...
"$SCRIPT" fichier*.txt --output-dir "$TMPDIR"/output-skeletized
"$SCRIPT" --strip fichier*.txt --output-dir "$TMPDIR"/output-stripped

# # (but don't consider excluded paths)
# grep -l -E 'EXCLUDED? FROM' fichier*.txt | (
#     cd "$TMPDIR"/output-stripped && xargs rm )

cd "$TMPDIR"

# ... and try to rebuild the skeletons from it. The result may be
# different from the source, but should produce the same results when
# ran through generate-skeletons.py again ...
cp -r output-skeletized/ output-rebuilt-skeletons/
diff -u -N output-stripped/ output-rebuilt-skeletons/ | "$DIFF2SKELL" --inplace

# ... so this should re-output the same as the first run:
(cd output-rebuilt-skeletons &&
     "$SCRIPT" fichier*.txt --output-dir ../output-rebuilt-skeletized)
(cd output-rebuilt-skeletons &&
     "$SCRIPT" fichier*.txt --output-dir ../output-rebuilt-stripped --strip)

# Actual check that both runs provide the same output
diff -ur output-rebuilt-skeletized/ output-skeletized/
diff -ur output-rebuilt-stripped/ output-stripped/

cd "$SCRIPTDIR"
rm -fr "$TMPDIR"
