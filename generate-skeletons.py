#!/usr/bin/env python3

# Find the latest version of this program at
# https://gitlab.com/moy/generate-skeletons


# BSD 2-Clause License
#
# Copyright (c) 2017, Matthieu Moy and Guillaume Salagnac
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import re
import sys
import os
import argparse
import fnmatch
import shutil


if sys.version_info[0] < 3:
    print("Python 2 is not supported. Please use Python 3.")
    sys.exit(1)

__version__ = '0.99'


STRIP = 'STRIP'
SKELETIZE = 'SKELETIZE'


CSI = "\x1B["
RED = CSI + "31;40m"
NORMAL = CSI + "0m"


def make_parser():
    parser = argparse.ArgumentParser(description="""
        Takes a list of files, like foo/bar.txt and foo/boz.txt
        and generates squeleton-ified versions, like
        DESTDIR/foo/bar.txt and DESTDIR/foo/boz.txt""")
    parser.add_argument(
        "--output-dir", "-o",
        help="Output directory (where files will be generated)",
        metavar="DIR", type=str)
    parser.add_argument(
        "--output-file",
        help="Output file (where skeleton will be generated). "
             "Only valid in one input file is provided.",
        metavar="FILE", type=str)
    parser.add_argument(
        "--create", "-c",
        action='store_true',
        default=False,
        help="Create output directory if it is not already present")
    parser.add_argument(
        "--strip", "-s",
        action='store_true',
        default=False,
        help="Strip annotations from source file (but keep skeletized parts). "
             "Don't exclude any file.")
    parser.add_argument(
        "--version",
        action='store_true',
        default=False,
        help="Print the version of generate-skeletons.py")
    parser.add_argument(
        "--cut-only",
        action="append",
        default=[],
        help="Cut only some versions "
        "(marked with /* BEGIN CUT (version1) */). "
        "Use --cut-only - to cut no versioned annotations. "
        "Use multiple times (--cut-only foo --cut-only bar) "
        "to cut several versions.")
    parser.add_argument(
        "--raw-copy-pattern",
        metavar="PATTERN",
        action="append",
        default=[],
        help="Files matching this pattern will be copied directly, ignoring "
        "annotations (e.g. to copy binary files). Warning: even files "
        "containing EXCLUDE FROM SKELETON directives will be copied.")
    parser.add_argument(
        "--exclude-pattern",
        metavar="PATTERN",
        action="append",
        default=[],
        help="Files matching this pattern will be ignored")
    parser.add_argument(
        "--cut-unversioned",
        action='store_true',
        default=False,
        help="Cut unversionned annotations (marked with /* BEGIN CUT */). "
        "Useful in conjunction with --cut-only VERSION.")
    parser.add_argument(
        "--excerpts",
        action='store_true',
        default=False,
        help="Enable BEGIN/END COPY TO (file) directives.")
    parser.add_argument(
        "--strip-components",
        default=0,
        metavar="N", type=int,
        help="Strip N path components (e.g. if input is foo/bar/file.txt, "
        "--strip-components=1 will strip foo/bar in the output directory).")
    parser.add_argument(
        "FILES", nargs='*',
        help="Input files (containing skeletization directives)",
        type=str)
    return parser


def die(*msg):
    # Don't mix stdout and stderr
    sys.stdout.flush()
    print(*msg, file=sys.stderr)
    sys.exit(1)


def version_note(name, file_note):
    if file_note:
        return r'( *\((?P<' + name + r'>[^)]*)\))?'
    else:
        return r'( *\((?P<' + name + r'>[a-zA-Z0-9\._]*)\))?'


def cut_version(version, cut_only, cut_unversioned):
    if not cut_only:
        # No version specified, always cut
        return True
    if version is None or version == "":
        # A version to cut is specified, but this annotation doesn't
        # have one (either no (...) present, or "()").
        return cut_unversioned
    return version in cut_only


def pattern_in_comment(pat, bol=r'\s*?', no_start=False, no_end=False,
                       file_note=False):
    if file_note:
        note = 'file'
    else:
        note = 'version'

    line_comment = (
        r'\s*(#|//|--)'  # host language comment marker
        + r'\s*'
        + pat + version_note(note + '1', file_note)
        + r'\s*'       # nothing but whitespace until EOL
    )
    # host language comment marker, /* or (*
    if no_start:
        START_COMMENT = ''
    else:
        START_COMMENT = r'([(/]\*|<!--)'
    # host language end of comment marker, */ or *)
    if no_end:
        END_COMMENT = ''
    else:
        END_COMMENT = r'(\*[/)]|-->)'
    c_like_comments = (
        r'\s*'
        + START_COMMENT
        + r'\s*'
        + pat + version_note(note + '2', file_note)
        + r'\s*'
        + END_COMMENT
        + r'\s*'  # nothing but whitespace until EOL
    )
    return (
        "^(?P<bol>" + bol + ")" +
        "(" + line_comment + "|" + c_like_comments + ")$"
    )


class Output:
    def __init__(self, excerpts):
        self.main_output = []
        self.excerpts = dict()
        self.active_excerpts = set()
        self.enable_excerpts = excerpts

    def append(self, line):
        self.main_output.append(line)
        if not self.enable_excerpts:
            return
        for outfile in self.active_excerpts:
            self.excerpts[outfile].append(line)

    def add_excerpt(self, outfile, linenum):
        if outfile in self.active_excerpts:
            error("Two BEGIN COPY TO in a row on line", linenum)
        self.active_excerpts.add(outfile)
        if outfile not in self.excerpts:
            self.excerpts[outfile] = []

    def close_excerpt(self, outfile, linenum):
        if outfile not in self.active_excerpts:
            error("Unexpected END COPY TO on line", linenum)
        self.active_excerpts.remove(outfile)

    def close_main(self, destname, destdir, is_executable=False):
        if self.active_excerpts:
            error("BEGIN COPY TO directives not closed: {} at end of file."
                  .format(', '.join(self.active_excerpts)))
        dest = os.open(destname, os.O_WRONLY | os.O_CREAT,
                       0o777 if is_executable else 0o666)
        os.write(dest, ''.join(self.main_output).encode('utf-8'))
        os.close(dest)
        if self.enable_excerpts:
            for outfile in self.excerpts.keys():
                with open(os.path.join(destdir, outfile), "w") as dest:
                    dest.write(''.join(self.excerpts[outfile]))

    def fmt_excerpts(self):
        if self.excerpts.keys() and self.enable_excerpts:
            return ' (' + ', '.join(self.excerpts.keys()) + ')'
        else:
            return ''


def error(*msg):
    print(": ERROR")
    die(*msg)


def process_file(sourcename, destname, destdir,
                 mode, cut_only,
                 raw_copy_pattern, exclude_pattern,
                 cut_unversioned,
                 excerpts):

    print(sourcename, "->", destname, end='')

    if any(fnmatch.fnmatch(sourcename, pattern)
           for pattern in raw_copy_pattern):
        shutil.copyfile(sourcename, destname)
        print(": copied raw")
        return 0
    if any(fnmatch.fnmatch(sourcename, pattern)
           for pattern in exclude_pattern):
        print(RED + "Excluded from skeleton" + NORMAL)
        return 0

    source = open(sourcename)
    is_executable = os.access(sourcename, os.X_OK)
    output = Output(excerpts)
    removed_lines = 0
    # are we currently cutting stuff?
    cutting = False
    # Are we between BEGIN CUT and END CUT (even if we disabled cutting)?
    in_cut = False
    # are we in an BEGIN/END UNCOMMENT block?
    uncommenting = False
    # Are we between BEGIN UNCOMMENT and END UNCOMMENT
    # (even if we disabled cutting)?
    in_uncomment = False

    for linenum, line in enumerate(source, 1):
        m = re.match(pattern_in_comment('EXCLUDED? FROM SKELETON'), line)
        if m:
            version = m.group('version1') or m.group('version2')
            if mode == STRIP:
                # Don't exclude the file, we're generating the
                # (stripped) solution which contains all files.
                continue
            elif cut_version(version, cut_only, cut_unversioned):
                print(": " + RED + "Excluded from skeleton" + NORMAL)
                return 0

        m = re.match(pattern_in_comment(r'BEGIN COPY TO', file_note=True),
                     line)
        if m:
            outfile = m.group('file1') or m.group('file2')
            output.add_excerpt(outfile, linenum)
            continue

        m = re.match(pattern_in_comment(r'END COPY TO', file_note=True),
                     line)
        if m:
            outfile = m.group('file1') or m.group('file2')
            output.close_excerpt(outfile, linenum)
            continue

        m = re.match(pattern_in_comment(r'(START|BEGIN) CUT'), line)
        if m:
            if cutting:
                error("Unexpected BEGIN CUT on line", linenum)
            else:
                version = m.group('version1') or m.group('version2')
                if cut_version(version, cut_only, cut_unversioned):
                    cutting = True
                in_cut = True
                continue
        if re.match(pattern_in_comment('END CUT'), line):
            if in_cut:
                cutting = False
                in_cut = False
                continue
            else:
                error("Unexpected END CUT on line", linenum)

        m = re.match(pattern_in_comment(r'(START|BEGIN) UNCOMMENT',
                                        no_end=True),
                     line)
        if m:
            if in_uncomment:
                error("Two BEGIN UNCOMMENT in a row on line", linenum)
            else:
                version = m.group('version1') or m.group('version2')
                if (
                        not cutting
                        and cut_version(version, cut_only, cut_unversioned)
                        and mode == SKELETIZE
                ):
                    cutting = False
                else:
                    cutting = True
                in_uncomment = True
                uncommenting = True
                continue

        m = re.match(pattern_in_comment(r'END UNCOMMENT',
                                        no_start=True),
                     line)
        if m:
            if in_uncomment:
                uncommenting = False
                in_uncomment = False
                if not in_cut:
                    cutting = False
                continue
            else:
                error("END UNCOMMENT without BEGIN UNCOMMENT on line", linenum)

        # UNCOMMENT blocks
        if mode == STRIP and uncommenting:
            continue

        # deal with: foo /* UNCOMMENT this will be uncommented */ bar
        c_like_comments = (
            r'(?P<startofline>^.*)'  # BOL whitespace or other characters
            + r'([/(]\*|<!--)'       # Beginning of comment, /* or (* or <!--
            + r'\s*'
            + 'UNCOMMENT' + version_note('version', False)
            + r' *'
            + '(?P<content>.*?)'     # host language text to be uncommented
            + r' *(\*[/)]|-->)'      # end of comment, */ or *) or -->
            + r'(?P<restofline>.*)$'
        )
        m = re.match(c_like_comments, line)
        if m:
            if (
                not cutting and
                mode == SKELETIZE and
                cut_version(m.group('version'), cut_only,
                            cut_unversioned)
            ):
                output.append(m.group('startofline') +
                              m.group('content') +
                              m.group('restofline') + '\n')
            else:
                s = m.group('startofline')
                e = m.group('restofline').strip()
                if e == '':
                    s = s.rstrip()
                output.append(s + e + '\n')
            continue

        # deal with: foo # UNCOMMENT bar
        line_comments = (
            r'(?P<startofline>^.*)'  # BOL whitespace or other characters
            + '(#|//|--)'
            + r'\s*'
            + 'UNCOMMENT' + version_note('version', False)
            # Allow # UNCOMMENT (foo)text and # UNCOMMENT (foo) text,
            # but strip at most one space to allow
            # "UNCOMMENT (foo)      text" to keep indentation before "text".
            + r' ?'
            # host language text to be uncommented
            + '(?P<restofline>.*)$'
        )
        m = re.match(line_comments, line)
        if m:
            if (
                    not cutting and
                    mode == SKELETIZE and
                    cut_version(m.group('version'), cut_only,
                                cut_unversioned)
               ):
                output.append(m.group('startofline') +
                              m.group('restofline') + '\n')
            else:
                start_line = m.group('startofline').rstrip()
                if start_line != "":
                    output.append(start_line + '\n')
            continue

        m = re.match(pattern_in_comment("TO DELETE", bol='.*?'), line)
        if m:
            version = m.group('version1') or m.group('version2')
            if mode == STRIP or not cut_version(version, cut_only,
                                                cut_unversioned):
                output.append(m.group('bol') + '\n')
            else:
                removed_lines += 1
            continue

        # if we're here, then `line' contains no skeletization command
        if mode == STRIP or not cutting:
            output.append(line)
        else:
            removed_lines += 1

    # file is finished
    if in_cut:
        error("Unexpected end of file (START CUT not closed by END CUT)")

    # file is finished
    if in_uncomment:
        error("Unexpected end of file "
              "(START UNCOMMENT not closed by END UNCOMMENT)")

    output.close_main(destname, destdir, is_executable)

    print(output.fmt_excerpts(), end=': ')
    if removed_lines > 0:
        msg = str(removed_lines) + " line(s) removed"
    else:
        msg = "No line removed"
    if is_executable:
        msg += " (executable)"
    print(RED + msg + NORMAL)
    return removed_lines


def parse_cli():
    parser = make_parser()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()

    if args.version:
        print("generate-skeletons.py version", __version__)
        sys.exit(0)

    if len(args.FILES) == 0:
        die("Please provide at least one input file")

    if len(args.FILES) > 1 and args.output_file:
        die("Only one input file allowed with --output-file")

    if args.strip_components > 0 and args.output_file:
        die("--strip-components and --output-file are incompatible")

    if args.strip_components < 0:
        die("Please provide a positive argument for --strip-components.")

    if not (args.output_file or args.output_dir):
        die("Please provide either --output-file or --output-dir.")
    return args


def main():
    args = parse_cli()

    if args.strip:
        mode = STRIP
    else:
        mode = SKELETIZE

    for name in args.FILES:
        if not os.path.isfile(name):
            if os.path.isdir(name):
                die("path is a directory, unsupported: '" + name + "'")
            elif not os.path.exists(name):
                die("path does not exist: '" + name + "'")
            else:
                die("not a file: '" + name + "'")

    # --output-file mode
    if args.output_file:
        if args.output_dir:
            destdir = args.output_dir.rstrip("/")
        else:
            destdir = '.'
        process_file(args.FILES[0], args.output_file,
                     destdir, mode, args.cut_only,
                     args.raw_copy_pattern, args.exclude_pattern,
                     args.cut_unversioned,
                     args.excerpts)
        return

    # --output-dir mode
    assert args.output_dir
    destdir = args.output_dir
    destdir = destdir.rstrip("/")  # trailing slashes are a nuisance

    if args.create and not os.path.exists(destdir):
        os.makedirs(destdir)
    if not os.path.isdir(destdir):
        die("no such directory:", destdir)

    removed_lines = 0
    for sourcename in args.FILES:

        def parts(path):
            p, f = os.path.split(path)
            return parts(p) + [f] if f else [p]
        split_stripped_source = parts(sourcename)[args.strip_components + 1:]
        if split_stripped_source == []:
            die("Cannot strip {} components from path {}.",
                args.strip_components, sourcename)
        stripped_sourcename = os.path.join(*split_stripped_source)
        destname = os.path.join(destdir, stripped_sourcename)
        destsubdir = os.path.dirname(destname)
        if not os.path.exists(destsubdir):
            os.makedirs(destsubdir)
        removed_lines += process_file(sourcename,
                                      destname,
                                      destdir,
                                      mode,
                                      args.cut_only,
                                      args.raw_copy_pattern,
                                      args.exclude_pattern,
                                      args.cut_unversioned,
                                      args.excerpts)
    print(RED + "Total: {} line(s) removed.".format(removed_lines) + NORMAL)


if __name__ == '__main__':
    main()
