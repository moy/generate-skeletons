#! /usr/bin/env python

from setuptools import setup
import re

NAME = 'generate-skeletons'
URL = 'https://gitlab.com/moy/' + NAME


def read_version():
    gs = __import__(NAME)
    return gs.__version__


def read_readme():
    readme = open('README.rst').read()
    if hasattr(readme, 'decode'):
        # In Python 3, turn bytes into str.
        readme = readme.decode('utf8')
    # turn relative links into absolute ones
    readme = re.sub(r'`<([^>]*)>`__',
                    r'`\1 <' + URL + r"/blob/master/\1>`__",
                    readme)
    return readme


setup(
    name=NAME,
    version=read_version(),
    description='Extract skeletons from source code',
    long_description=read_readme(),
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        ],
    keywords='skeletons',
    url=URL,
    author='Matthieu Moy and Guillaume Salagnac',
    maintainer='Matthieu Moy',
    maintainer_email='git@matthieu-moy.fr',
    install_requires=['unidiff'],
    license='BSD',
    scripts=[NAME + '.py', 'diff2skel.py'],
    )
